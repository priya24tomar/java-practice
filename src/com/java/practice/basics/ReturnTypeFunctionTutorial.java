package com.java.practice.basics;

public class ReturnTypeFunctionTutorial {
	public static void main(String args[]) {
		ReturnTypeFunctionTutorial returnType = new ReturnTypeFunctionTutorial();
returnType.add();
	}
	// No Return Type Method Syntax
	// void add() {
	//
	// }

	// Return Type Method Syntax

	String add() {
		int c = 5 + 6;
		return "Sum of" + c;
	}

}
