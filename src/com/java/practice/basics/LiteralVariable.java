package com.java.practice.basics;

public class LiteralVariable {
	static boolean b1; // Global variable not required to initialized
	public static void main(String[] args) {
		//Literal Variable in java 
		//local variable declared within block or any function
        //local value must be initialized otherwise compile time error show
		//Literals never accept null value
//		int local;
//		System.out.println("Default Value of int :"+ local);
		int a=1;
		System.out.println(" Value of int :"+ a);
		long l=5221;
		System.out.println(" Value of long :"+ l);
		double d=2.55;
		System.out.println(" Value of double :"+ d);
		float f=2.55f; // Compulsory "f" after initialized variable for float
		System.out.println(" Value of float :"+ f);
		boolean b2= true;		
		System.out.println(" Value of boolen local with initiliasation :"+ b2);
		System.out.println("Default Value of boolean globle variable without initiliasation :"+ b1);// Default value of boolean is false
	}

}
