package com.java.practice.series;

public class FibonacciSum extends Fibonacci{
	static Integer sum=0;
	public static void main(String arg[]) {
		Fibonacci.getValue();
		Fibonacci.getFibonacci().forEach((fs)->{
			sum=sum+fs;
		});
		System.out.println("Sum Of Fibonacci= "+sum);
	}
}
