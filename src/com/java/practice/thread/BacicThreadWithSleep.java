package com.java.practice.thread;

// Thread is light weight process
// Use for achieve parallel process 
//

public class BacicThreadWithSleep {
	public static void main(String arg[]) {

		One one = new One();
		Two two = new Two();
		one.show();
		two.show();

	}

}

class One {

	void show() {
		for (int i = 0; i <= 10; i++) {
			System.out.println("1");
			try{Thread.sleep(500);}catch(Exception e) {}
		}
	}
}

class Two {

	void show() {
		for (int i = 0; i <= 10; i++) {
			System.out.println("2");
			try{Thread.sleep(500);}catch(Exception e) {}
		}
	}
}
