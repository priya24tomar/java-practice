package com.java.practice.thread;

// Thread is light weight process
// Use for achieve parallel process 
//

public class ParallelThreadUsingRunnableInterface {
	public static void main(String arg[]) {

		OneIn one = new OneIn();
		TwoIn two = new TwoIn();
		Thread t1 = new Thread(one);
		t1.start();
		//one.start(); show error
		//two.start(); show error 
		//we can not call start() method directly by object ref because In Runnable Interface no 
		// any method name start() show firstly we create tread object with passing constructor value
		// after that call start() method by thread object ref.
		try {
			Thread.sleep(10);
		} catch (Exception e) {
		}
		Thread t2 = new Thread(two);
		t2.start();
		

	}

}

class OneIn implements Runnable {

	public void run() {
		for (int i = 0; i <= 10; i++) {
			System.out.println("1");
			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}
		}
	}
}

class TwoIn implements Runnable {

	public void run() {
		for (int i = 0; i <= 10; i++) {
			System.out.println("2");
			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}
		}
	}
}
