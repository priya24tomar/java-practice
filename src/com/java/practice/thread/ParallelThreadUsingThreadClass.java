package com.java.practice.thread;

// Thread is light weight process
// Use for achieve parallel process 
//

public class ParallelThreadUsingThreadClass {
	public static void main(String arg[]) {

		OneP one = new OneP();
		TwoP two = new TwoP();
		one.start();
		try {
			Thread.sleep(10);
		} catch (Exception e) {
		}
		two.start();

	}

}

class OneP extends Thread {

	public void run() {
		for (int i = 0; i <= 10; i++) {
			System.out.println("1");
			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}
		}
	}
}

class TwoP extends Thread {

	public void run() {
		for (int i = 0; i <= 10; i++) {
			System.out.println("2");
			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}
		}
	}
}
