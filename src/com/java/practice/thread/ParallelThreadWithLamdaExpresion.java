package com.java.practice.thread;

// Thread is light weight process
// Use for achieve parallel process 
//

public class ParallelThreadWithLamdaExpresion {
	public static void main(String arg[]) {

		// Runnable one = () -> {
		// for (int i = 0; i <= 10; i++) {
		// System.out.println("1");
		// try {
		// Thread.sleep(500);
		// } catch (Exception e) {
		// }
		// }
		// };
		// Use anonymous class to code optimization and lamda expression
		// Runnable two = () -> {
		// for (int i = 0; i <= 10; i++) {
		// System.out.println("2");
		// try {
		// Thread.sleep(500);
		// } catch (Exception e) {
		// }
		// }
		// };
//		we are passing one and two object directly in  Thread constructor
		Thread t1 = new Thread(() -> {
			for (int i = 0; i <= 10; i++) {
				System.out.println("1");
				try {
					Thread.sleep(500);
				} catch (Exception e) {
				}
			}
		});
		t1.start();
		// one.start(); show error
		// two.start(); show error
		// we can not call start() method directly by object ref because In Runnable
		// Interface no
		// any method name start() show firstly we create tread object with passing
		// constructor value
		// after that call start() method by thread object ref.
		try {
			Thread.sleep(10);
		} catch (Exception e) {
		}
		Thread t2 = new Thread(() -> {
			for (int i = 0; i <= 10; i++) {
				System.out.println("2");
				try {
					Thread.sleep(500);
				} catch (Exception e) {
				}
			}
		});
		t2.start();

	}

}