package com.java.practice.exception;

public class ThrowForcelyException {

	public static void main(String[] args) {
		int i, j, k = 0;
		i = 5;
		j = 0;
		try {
			k = i + j;
			if (k < 6)
				throw new ArithmeticException();
		} catch (ArithmeticException e) {
			System.out.println("Minimum Value is 5");
		} catch (Exception e) {
			System.out.println("For Unknown Exception.");
		}

		System.out.println(k);
	}

}
