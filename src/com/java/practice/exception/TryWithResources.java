package com.java.practice.exception;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TryWithResources {

	public static void main(String[] args) throws IOException {
		int i, j, k = 0;
		i = 5;
		j = 1;

		// if we use try with resources we don't need to
		// catch and finally block
		// its remove resource after use
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
			System.out.println("Enter no");
			j = Integer.parseInt(br.readLine());
			k = i / j;
		}
		System.out.println(k);
	}

}
