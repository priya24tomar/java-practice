package com.java.practice.exception;

public class ExceptionHandlingCatchHierarchy {

	public static void main(String[] args) {
		int i, j, k = 0;
		i = 5;
		j = 0;// Change value of j, 0 to 2 for check second catch message
		int l[] = new int[] { 1, 2, 3, 4 };
		try {
			k = i / j;
			System.out.println(l[5]);
		}
		// If we catch exception by parent class first
		// and after that catch child class it show
		// compile time error uncomment bottom 2 line
		// to check
		// catch (Exception e) {
		// System.out.println("For Unknown Exception.");
		// }

		catch (ArithmeticException e) {
			System.out.println("Can't not devide by zero");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Element you find not available.");
		}
		// If we catch multy time same exception
		// Compiler show already handled this exception
		// uncomment bottom 2 line to check
		// catch (ArrayIndexOutOfBoundsException e) {
		// System.out.println("Element you find not available.");
		// }

		catch (Exception e) {
			System.out.println("For Unknown Exception.");
		}

		System.out.println(k);
	}

}
