package com.java.practice.exception;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FinallyBlockWithTryCatch {

	public static void main(String[] args) {
		
		int i, j, k = 0;
		i = 5;
		j = 1;// Change value of j, 0 to 2 for check second catch message

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		// if we uncomment line no 17 and comment
		// line no 20 it show compile time exception
		// this type exception is call checked exception
		// j = Integer.parseInt(br.readLine());
		try {
			System.out.println("Enter no");
			j = Integer.parseInt(br.readLine());
			k = i / j;
		} catch (IOException e) {
			System.out.println("Checked Exception");
		} catch (ArithmeticException e) {
			System.out.println("Can't not devide by zero");
		} catch (Exception e) {
			System.out.println("Unknown Exception.");
		} finally {
			// This block always run at the last
			System.out.println("Bye");
		}
		System.out.println(k);
	}

}
