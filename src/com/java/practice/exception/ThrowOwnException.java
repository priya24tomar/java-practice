package com.java.practice.exception;

import com.java.practice.support.MyOwnException;

public class ThrowOwnException {

	public static void main(String[] args) {
		int i = 10, j = 5, k = 0;
		k = i + j;
		try {
			if (k > 6)
				throw new MyOwnException("Mera Wala Error Hai");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
