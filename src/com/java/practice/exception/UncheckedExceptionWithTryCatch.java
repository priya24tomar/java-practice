package com.java.practice.exception;

public class UncheckedExceptionWithTryCatch {

	public static void main(String[] args) {
		int i, j, k = 0;
		i = 5;
		j = 0;
		try {
			k = i / j;
		} catch (Exception e) {
			System.out.println("Can't not devide by zero");
		}
		System.out.println(k);
	}

}
