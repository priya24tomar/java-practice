package com.java.practice.exception;

public class CreateOwnException {

	public static void main(String[] args) {
		int i = 5;
		if (i < 6) {
			try {
				throw new OwnException("My Exception Called");
			} catch (OwnException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
	}

}

class OwnException extends Throwable {
	public OwnException(String s) {
		super(s);
	}
}