package com.java.practice.exception;

public class Plane {
	static String s = "-";

	public static void main(String[] args) {

		new Plane().s1();
		System.out.println(s);

	}

	void s1() {
		try {
			s2();
		} catch (Exception e) {
			s += "c";
		}
	}

	void s2() throws Exception {

		try {
			s3();
		} catch (Exception e) {
			s += "2";
		}
		s3();
		s += "2b";

	}

	void s3() throws Exception {
		try {
			throw new Throwable();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
