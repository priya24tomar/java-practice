package com.java.practice.collection;

import com.java.practice.support.HashCodeEqualsSupport;

public class HashCodeAndEquals {

	public static void main(String[] args) {
		HashCodeEqualsSupport hces1 = new HashCodeEqualsSupport("Nitish", 1, 500);
		HashCodeEqualsSupport hces2 = hces1;

		if (hces1.equals(hces2) && hces1.hashCode() == hces2.getId()) {
			System.out.println(
					"Both Object are same by hash code and object called First Return value of overrided equals method");
		}

		HashCodeEqualsSupport hces3 = new HashCodeEqualsSupport("Nitish Singh", 2, 500);
		HashCodeEqualsSupport hces4 = new HashCodeEqualsSupport("Nitish", 2, 500);

		if (hces3.equals(hces4) && hces3.hashCode() == hces4.getId()) {
			System.out.println(
					"Both Object are same by hash code and object id called Second Return value of overrided equals method");
		}

	}

}
