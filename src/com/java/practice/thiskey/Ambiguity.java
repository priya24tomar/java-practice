package com.java.practice.thiskey;

public class Ambiguity {

	int id;
	String name;

	Ambiguity(int id, String name) {
		id = id; // here argument variable id and class level variable id same
					// name so
					// argument id variable assign to himself id which is
					// pass as argument in constructor, not identify the class
					// variable id
					// without this key word
		name = name; // here argument variable name and class level variable
						// name same name so argument name variable assign to
						// himself name which is pass as argument in
						// constructor, not identify the class variable
						// name without this key word
	}

	public Ambiguity(String name, int id) {
		this.id = id;// here argument id value assign to class variable id, this
						// key word is identify the class label name variable
		this.name = name;// here argument name value assign to class variable
							// name, this key word is identify the class label
							// name variable
	}

	void show() {
		System.out.println("Id: " + id + "\t Name: " + name);
	}

	public static void main(String[] args) {
		Ambiguity a1 = new Ambiguity(1, "Nitish");

		a1.show();
		Ambiguity a2 = new Ambiguity("Pradeep", 1);
		a2.show();

	}

}
