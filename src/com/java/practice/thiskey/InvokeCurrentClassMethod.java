package com.java.practice.thiskey;

public class InvokeCurrentClassMethod {

	public static void main(String[] args) {
		InvokeCurrentClassMethod iccm = new InvokeCurrentClassMethod();
		iccm.call();

	}

	void call() {
		System.out.println(
				"If we are not added this key word Automatically add this keyword before method like= \"this.secondMethod();\" when call second method");
		secondMethod();
	}

	void secondMethod() {
		System.out.println("Called");
	}

}
