package com.java.practice.thiskey;

public class ThisAsReturnValue {

	ThisAsReturnValue getCurrentObject() {
		return this;
	}

	public static void main(String[] args) {
		ThisAsReturnValue trv = new ThisAsReturnValue();
		System.out.println(trv);
		System.out.println(trv.getCurrentObject());

	}

}
