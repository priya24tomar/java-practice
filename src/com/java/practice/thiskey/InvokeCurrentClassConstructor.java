package com.java.practice.thiskey;

public class InvokeCurrentClassConstructor {

	int id;
	String name;

	public InvokeCurrentClassConstructor(int id, String name) {
		this();
		System.out.println("Id: " + id + "\t Name: " + name);
		System.out.println("Called Argument constructor by this keyword");

	}

	public InvokeCurrentClassConstructor(String name, int id) {
		System.out.println("Id: " + id + "\t Name: " + name);

	}

	public InvokeCurrentClassConstructor() {
		//Called Argument constructor by this keyword
		this("Pradeep", 2);
		System.out.println(
				"Call Default Constructor with this key word on another constructor");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new InvokeCurrentClassConstructor(1, "Nitish");

	}

}
