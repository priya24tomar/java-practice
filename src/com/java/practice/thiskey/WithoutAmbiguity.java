package com.java.practice.thiskey;

public class WithoutAmbiguity {
	int id;
	String name;

	WithoutAmbiguity(int rid, String rname) {
		id = rid; // here argument value is differ then class level variable so
					// no need of this keyword
		name = rname;
	}

	void show() {
		System.out.println("Id: " + id + "\t Name: " + name);
	}

	public static void main(String[] args) {
		WithoutAmbiguity a1 = new WithoutAmbiguity(1, "Nitish");

		a1.show();
		WithoutAmbiguity a2 = new WithoutAmbiguity(2, "Pradeep");
		a2.show();

	}

}
