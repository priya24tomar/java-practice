package com.java.practice.thiskey;

public class ThisAsArgument {

	void call() {
		thisAsArgument(this);// This as argument
	}

	void thisAsArgument(ThisAsArgument obj) {
		System.out.println(obj.name);
	}

	public static void main(String[] args) {
		ThisAsArgument taa = new ThisAsArgument();
		taa.name = "Nitish";
		taa.call();
	}

	String name;

}
