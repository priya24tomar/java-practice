package com.java.practice.statice;

public class WithoutStatic {

	// Java Program to demonstrate the use of an instance variable
	// which get memory each time when we create an object of the class.

	int count = 0;// will get memory each time when the instance is created

	WithoutStatic() {
		count++;// incrementing value
		System.out.println(count);
	}

	public static void main(String[] args) {
		{
			// Creating objects
			WithoutStatic c1 = new WithoutStatic();
			WithoutStatic c2 = new WithoutStatic();
			WithoutStatic c3 = new WithoutStatic();
			new WithoutStatic();
			new WithoutStatic();
			new WithoutStatic();
		}

	}

}
