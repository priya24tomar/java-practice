package com.java.practice.statice;

public class StaticBlock {
	// static block use to initialize static variable its invoke automatically
	// before main method at the time of loading class
	static {
		System.out.println("Static block");
	}

	public static void main(String[] args) {
		System.out.println("Main");
	}
}
