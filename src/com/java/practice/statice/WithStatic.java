package com.java.practice.statice;

public class WithStatic {
	// Java Program to illustrate the use of static variable which
	// is shared with all objects.

	static int i = 0; // will get memory only once and retain its value

	WithStatic() {
		i++; // incrementing the value of static variable
		System.out.println(i);
	}

	public static void main(String[] args) {
		new WithStatic();
		new WithStatic();
		new WithStatic();
	}

}
