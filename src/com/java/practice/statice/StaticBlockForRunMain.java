package com.java.practice.statice;

public class StaticBlockForRunMain {
// Its possible to run program without main till jdk 1.6 but after jdk 1.7 its not allowed.
	static {
		System.out.println("Static Block");
		run();
	}
	
	static void run() {
		System.out.println("Run");
	}
}
