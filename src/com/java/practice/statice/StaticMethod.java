package com.java.practice.statice;

public class StaticMethod {
	// Java Program to demonstrate the use of a static method.
	private int id;
	private String name;

	static String collage = "RKGIT";

	StaticMethod(int id, String name) {
		this.id = id;
		this.name = name;
	}

	// static method to change the value of static variable
	static void changeCollege() {
		collage = "FGIT";
	}

	// normal method to display values
	void display() {
		System.out.println(
				"Id: " + id + "\t Name: " + name + "\t College: " + collage);
	}

	public static void main(String[] args) {

		StaticMethod s1 = new StaticMethod(1, "Nitish");
		s1.display();
		StaticMethod.changeCollege();
		StaticMethod s2 = new StaticMethod(2, "Pradeep");
		s2.display();
	}

}
