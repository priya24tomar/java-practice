package com.java.practice.oops;

// Getting one class feature to another is called  inheritance 

//Type:  1) Single Level 2) Multilevel 3) Multiple its not supported in java
//1)		A	_
//			|	 |
//			|	 |	Single Level
//			B	_|
//
//2)		A	_
//			|	 |
//			|	 |	Multilevel
//			B	 |
//			|	 |
//			|	 |	
//			C	_|
//3) 		A			B
//			|			|
//			|_____C_____| Multiple
//
//Multiple inheritance not support in java bcoz of if same method have
//in A and B class and inherited by C which method called 

// Why We need to Inheritance Concept
// To code re usability EX:=
// If we have a class which has an method add and that class will be non editable
// and we required to add a new method called sub than we have three solution
// 1) create another class which have to method add & sub and use that
// 2) create another class which has sub method and create both class object
// to achieve our goal that is bad way to write code
// 3) Using inheritance concept like, child has feature of his/her parents
// call child and access his/her feature bcoz child inherit parent feature
public class Inharitance {

	public static void main(String[] args) {
		// This object Achieve single inheritance
		SingleLevelInheritanceAchiveClass singleLevel = new SingleLevelInheritanceAchiveClass();
		System.out.println("Called by singlelevel");
		singleLevel.add();
		// we can't call add method until we inherit IAdd class in IAddSub
		// using extends keyword.
		// To check remove extends and IAdd class name from IAddSub class
		singleLevel.sub();

		// Achieve Multilevel Inheritance
		MultiLevelLevelInheritanceAchiveClass multiLevel = new MultiLevelLevelInheritanceAchiveClass();
		System.out.println("Called by multilevel");
		multiLevel.add();
		multiLevel.sub();
		multiLevel.multi();

	}

}

class IAdd {

	void add() {
		System.out.println(5 + 2);
	}

}

class SingleLevelInheritanceAchiveClass extends IAdd {
	// This class inherit IAdd Class only single class called SingleLevel
	void sub() {
		System.out.println(5 - 2);
	}
}

class MultiLevelLevelInheritanceAchiveClass extends SingleLevelInheritanceAchiveClass {
	// This class inherit SingleLevelInheritanceAchiveClass Class
	// SingleLevelInheritanceAchiveClass inherit IAdd Class
	// called MultiLevel
	void multi() {
		System.out.println(5 * 2);
	}
}

// Note: base parent supper this all call upper class respect to extended class
// Derived child sub these all called lower class respect to upper class
