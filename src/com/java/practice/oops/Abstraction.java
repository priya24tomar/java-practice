package com.java.practice.oops;

// Theory
// Method have 2 thing 1) Definition or Behavior 2) Declaration
// 1) public void add(){
// Method Definitions is here.
// }
// 2) public abstract void add(); declared method
// a) If we declared a method only that method must be abstract 
// b) In which class abstract method declared class also must be abstract
// c) We can't create object of abstract class
// d) Abstract class can have and can't have abstract method
// Why need Abstract Concept
// When we don't know definition of method some else use that define
// but he/she don't know declaration of method we use abstract method

public class Abstraction {

	public static void main(String[] args) {
		// Can't create object of CallFeatureEnabled & MovingFeatureEnabled
		// bcoz of both is abstract class
		// CallFeatureEnabled cfe= new CancellationException();

		AllFeatureEnabled afe = new AllFeatureEnabled();
		afe.call();
		afe.move();
		afe.songPlay();
		afe.videoCall();

	}

}

abstract class CallFeatureEnabled {
	public void call() {
		System.out.println("Calling Feature Available");
	}

	public abstract void move();

	public abstract void songPlay();

	public abstract void videoCall();

}

abstract class MovingFeatureEnabled extends CallFeatureEnabled {
	public void move() {
		System.out.println("Moving Feature Available");
	}
}

class AllFeatureEnabled extends MovingFeatureEnabled {
	public void songPlay() {
		System.out.println("Song Feature Available");
	}

	public void videoCall() {
		System.out.println("Video Feature Available");
	}
}
