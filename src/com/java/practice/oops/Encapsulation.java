package com.java.practice.oops;

/////////Encapsulation or Data Hiding/////////
//When we get private variable by using getter setter methods
//That time we achieve the Encapsulation.

public class Encapsulation {
	public static void main(String arg[]) {

		A obj = new A();// Creating an object reference
		obj.normalVariable = 4;
		// here we can access public or default variable directly
		// but our variable is disclose for every one who use object of A
		// Fail Encapsulation Concept

		// obj.privateVariable=5;
		// On the above line we got error that variable is not visible bcoz of
		// Class A variable named privateVarivable is private and private variable
		// can't access out side of class, To check uncomment line 15 and mouse
		// hover on line 15 it show the error. Now problem is how to access private
		// variable than we use Encapsulation concept.

		obj.setPrivateVariable(5);
		System.out.println(obj.getPrivateVariable());
		// on above 2 line we achieve encapsulation using getter setter method

	}
}

class A {
	int normalVariable;
	private int privateVariable;

	public int getPrivateVariable() {
		return privateVariable;
	}

	public void setPrivateVariable(int privateVariable) {
		this.privateVariable = privateVariable;
	}

}