package com.java.practice.oops;

// Every class have a constructor either we create or not called 
// Default constructor
// public B() {
//     super();
// }
// constructor is member method which is same as class name
// constructor never return anything that because no return type need
// and always public and first line is super
// Use to allocate memory and initialize value
// It will call automatically by JVM when create object 
// no need to call like method
public class Constructor {

	public static void main(String s[]) {

		// Create Object for Default Constructor
		B obj = new B();

		// Create Object for Overloaded Constructor
		B obj1 = new B(5);
	}

}

class B {
	int a;
	float b;

	public B() {
		System.out.println("Default Constructor Called");
	}

	public B(int a) {
		System.out.println("Overloaded Constructor called :" + a);
	}

}
