package com.java.practice.oops;

// Polymorphism Overriding
//Poly = Many , Morphism = Behavior 
//For Overriding achieve using following cases
// Same name of method and same parameters or argument
// But method in different class
// static method can't be override but can be overload
public class PolymorphismOverriding {

	public static void main(String[] args) {
		// OverridingB pol = new OverridingB();
		// pol.show(2);

		OverridingA oA = new OverridingB();
		// here we override non static method
		System.out.println("Non static Override Is Print Override Method from Class B");
		oA.show(6);
		System.out.println("Static Override Is Print Override Method from Class A");
		System.out.println(oA.hide(5));

	}
}

class OverridingA {
	void show(int a) {
		System.out.println("Class OverridingA Show Method  :" + a);
	}

	public static String hide(int a) {
		return "Static Method From Class A";
	}
}

class OverridingB extends OverridingA {
	// If we comment bottom 3 line main method call
	// OverridingA class method
	void show(int a) {
		System.out.println("Class OverridingB Show Method :" + a);
	}

	// Static method can't be override becoz it treat like new method
	public static String hide(int a) {
		return "Static Method From Class B";
	}
}