package com.java.practice.oops;

// Polymorphism Overloading
//Poly = Many , Morphism = Behavior 
//For Overloading achieve using following cases
// Same name of method and different parameters type or no of argument
public class PolymorphismOverloading {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PolymorphismOverloading pol = new PolymorphismOverloading();
		pol.show();
		pol.show(2);
	}

	void show() {
		System.out.println("Without Argument");
	}

	void show(int a) {
		System.out.println("No of Argument Overloaded Show Function :" + a);
	}
	
	void show(double a) {
		System.out.println("Type of Argument Overloaded :" + a);
	}

}
