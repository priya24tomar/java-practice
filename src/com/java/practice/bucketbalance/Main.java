package com.java.practice.bucketbalance;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Main {

	public static final Map<String, Float> instanceSizes = new HashMap<>();

	public static void main(String[] args) {
		loadInstanceSize();
		Map<String, Map<String, ResultInstance>> finalResultMap = new HashMap<>();
		Map<String, Integer> totalUsedInstances = loadTotalUsedInstances();
		Map<String, Integer> totalReservedIntances = loadTotalReservedInstances();
		finalResultMap = buildInitialFinalResultMap(totalUsedInstances,
				totalReservedIntances);

		System.out.println("Total Instances \n");
		totalUsedInstances.forEach((k, v) -> {
			System.out.println("Key: " + k + "Value: " + v);
		});
		System.out.println("Reserved Instances \n");
		totalReservedIntances.forEach((k, v) -> {
			System.out.println("Key: " + k + "Value: " + v);
		});

		System.out.println("Before Balance \n");

		finalResultMap.forEach((k, v) -> {
			System.out.println("Key: " + k);
			v.forEach((key, value) -> {
				System.out.println(
						"Key: " + key + " onDemand: " + value.getOndemand()
								+ " overHead: " + value.getOverhead()
								+ "\n \t \t" + "used: " + value.getUsed()
								+ " Reserved :" + value.getReserved());
			});
		});

		for (Entry<String, Map<String, ResultInstance>> rbm : finalResultMap
				.entrySet()) {
			if (!canWeBalanceFamily(rbm)) {
				for (Entry<String, ResultInstance> timap : rbm.getValue()
						.entrySet()) {
					timap.getValue().setOndemand(timap.getValue().getUsed()
							- timap.getValue().getReserved());
				}
			} else {
				Map<String, ResultInstance> familyBalancingMap = rbm.getValue()
						.entrySet().stream()
						.filter(x -> (x.getKey().contains(rbm.getKey())))
						.collect(Collectors.toMap(Map.Entry::getKey,
								Map.Entry::getValue));

				balanceBucket(familyBalancingMap);

				// System.out.println("Balnace Kar be");
				// familyMap.forEach((key, value) -> {
				// System.out.println(
				// "Key: " + key + " onDemand: " + value.getOndemand()
				// + " overHead: " + value.getOverhead()
				// + "\n \t \t" + "used: " + value.getUsed()
				// + " Reserved :" + value.getReserved());
				// });
			}
		}

		System.out.println("After Ballanced \n");
		finalResultMap.forEach((k, v) -> {
			System.out.println("Key: " + k);
			v.forEach((key, value) -> {
				System.out.println(
						"Key: " + key + " onDemand: " + value.getOndemand()
								+ " overHead: " + value.getOverhead()
								+ "\n \t \t" + "used: " + value.getUsed()
								+ " Reserved :" + value.getReserved());
			});
		});

	}

	static void balanceBucket(Map<String, ResultInstance> familyBalancingMap) {
		Float balancingAmount;
		for (Entry<String, ResultInstance> fbm : familyBalancingMap
				.entrySet()) {
			if (fbm.getValue().getOverhead() > 0) {
				String iSubType = fbm.getKey()
						.substring(fbm.getKey().indexOf(".") + 1);
				balancingAmount = instanceSizes.get(iSubType)
						* fbm.getValue().getOverhead();

				for (Entry<String, ResultInstance> innerFbm : familyBalancingMap
						.entrySet()) {
					if (innerFbm.getKey() != fbm.getKey()) {
						if (innerFbm.getValue().getOndemand() > 0) {
							float oldBal = balancingAmount;
							balancingAmount = balancingAmount - innerFbm
									.getValue().getOndemand()
									* instanceSizes.get(innerFbm.getKey()
											.substring(innerFbm.getKey()
													.indexOf(".") + 1));
							if (balancingAmount < 0) {
								fbm.getValue().setOverhead(0);
								innerFbm.getValue().setOndemand((int) (innerFbm
										.getValue().getOndemand()
										- (oldBal / instanceSizes.get(innerFbm
												.getKey()
												.substring(innerFbm.getKey()
														.indexOf(".") + 1)))));
							} else {
								fbm.getValue()
										.setOverhead((int) (balancingAmount
												/ instanceSizes.get(fbm.getKey()
														.substring(fbm.getKey()
																.indexOf(".")
																+ 1))));
								innerFbm.getValue().setOndemand(0);
							}
						}
					}
				}
			}
		}
	}

	private static boolean canWeBalanceFamily(
			Entry<String, Map<String, ResultInstance>> rbm) {
		boolean canWeBal = false;
		for (Entry<String, ResultInstance> timap : rbm.getValue().entrySet()) {
			if (timap.getValue().getReserved() > timap.getValue().getUsed())
				return true;
		}
		return canWeBal;
	}

	private static Map<String, Map<String, ResultInstance>> buildInitialFinalResultMap(
			Map<String, Integer> tui, Map<String, Integer> tri) {
		Map<String, Map<String, ResultInstance>> frm = new HashMap<>();
		for (Entry<String, Integer> trInstance : tri.entrySet()) {
			String familyName = trInstance.getKey().substring(0,
					trInstance.getKey().indexOf("."));
			int oh = 0;
			int od = 0;
			if (frm.isEmpty() || frm.get(familyName) == null) {
				Map<String, ResultInstance> ne = new HashMap<>();
				// calculateOhAndOd(trInstance.getValue(),
				// tui.get(trInstance.getKey()), oh, od);
				int value = trInstance.getValue()
						- tui.get(trInstance.getKey());
				if (value > 0)
					oh = value;
				else if (value < 0)
					od = Math.abs(value);
				ne.put(trInstance.getKey(),
						new ResultInstance(tui.get(trInstance.getKey()),
								trInstance.getValue(), od, oh));
				frm.put(familyName, ne);
			} else if (frm.get(familyName) != null
					&& !frm.get(familyName).isEmpty()) {
				Map<String, ResultInstance> ne = frm.get(familyName);
				// calculateOhAndOd(trInstance.getValue(),
				// tui.get(trInstance.getKey()), oh, od);
				int value = trInstance.getValue()
						- tui.get(trInstance.getKey());
				if (value > 0)
					oh = value;
				else if (value < 0)
					od = Math.abs(value);
				ne.put(trInstance.getKey(),
						new ResultInstance(tui.get(trInstance.getKey()),
								trInstance.getValue(), od, oh));
				frm.put(familyName, ne);
			}

		}

		return frm;
	}

	// private static void calculateOhAndOd(Integer reserved, Integer used, int
	// oh,
	// int od) {
	// int value = reserved - used;
	// if (value > 0)
	// oh = value;
	// else if (value < 0)
	// od = Math.abs(value);
	// }

	private static Map<String, Integer> loadTotalUsedInstances() {
		Map<String, Integer> totalUsedIntances = new HashMap<>();
		 totalUsedIntances.put("c3.4xlarge", 1);
		 totalUsedIntances.put("c3.2xlarge", 20);
		totalUsedIntances.put("m4.2xlarge", 4);
		totalUsedIntances.put("m4.large", 18);
		totalUsedIntances.put("m4.4xlarge", 8);
		 totalUsedIntances.put("m4.xlarge", 44);
		return totalUsedIntances;
	}

	private static Map<String, Integer> loadTotalReservedInstances() {
		Map<String, Integer> totalReservedIntances = new HashMap<>();
		 totalReservedIntances.put("c3.4xlarge", 6);
		 totalReservedIntances.put("c3.2xlarge", 9);
		totalReservedIntances.put("m4.2xlarge", 1);
		totalReservedIntances.put("m4.large", 1);
		totalReservedIntances.put("m4.4xlarge", 12);
		 totalReservedIntances.put("m4.xlarge", 12);
		return totalReservedIntances;
	}

	static void loadInstanceSize() {
		instanceSizes.put("small", 1f);
		instanceSizes.put("medium", 2f);
		instanceSizes.put("large", 4f);
		instanceSizes.put("xlarge", 8f);
		instanceSizes.put("2xlarge", 16f);
		instanceSizes.put("4xlarge", 32f);
		instanceSizes.put("8large", 64f);
		instanceSizes.put("10xlarge", 80f);
		instanceSizes.put("16xlarge", 128f);
		instanceSizes.put("32xlarge", 256f);
	}

}

class ResultInstance {

	private int used;
	private int reserved;
	private int ondemand;
	private int overhead;

	public ResultInstance() {
		// TODO Auto-generated constructor stub
	}

	public ResultInstance(int used, int reserved, int ondemand, int overhead) {
		super();
		this.used = used;
		this.reserved = reserved;
		this.ondemand = ondemand;
		this.overhead = overhead;
	}

	public int getUsed() {
		return used;
	}

	public void setUsed(int used) {
		this.used = used;
	}

	public int getReserved() {
		return reserved;
	}

	public void setReserved(int reserved) {
		this.reserved = reserved;
	}

	public int getOndemand() {
		return ondemand;
	}

	public void setOndemand(int ondemand) {
		this.ondemand = ondemand;
	}

	public int getOverhead() {
		return overhead;
	}

	public void setOverhead(int overhead) {
		this.overhead = overhead;
	}

}
