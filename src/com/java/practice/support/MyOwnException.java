package com.java.practice.support;

public class MyOwnException extends Exception {

	public MyOwnException(String msg) {
		super(msg);
	}
}
