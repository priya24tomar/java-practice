package com.java.practice.support;

public class HashCodeEqualsSupport {

	private String name;
	private int id;
	private int sallary;

	public HashCodeEqualsSupport(String name, int id, int sallary) {
		super();
		this.name = name;
		this.id = id;
		this.sallary = sallary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSallary() {
		return sallary;
	}

	public void setSallary(int sallary) {
		this.sallary = sallary;
	}
//
//  Default Overrieded method
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + id;
//		result = prime * result + ((name == null) ? 0 : name.hashCode());
//		result = prime * result + sallary;
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		HashCodeEqualsSupport other = (HashCodeEqualsSupport) obj;
//		if (id != other.id)
//			return false;
//		if (name == null) {
//			if (other.name != null)
//				return false;
//		} else if (!name.equals(other.name))
//			return false;
//		if (sallary != other.sallary)
//			return false;
//		return true;
//	}
//	
	
	

//	@Override
//	public boolean equals(Object obj) {
//		HashCodeEqualsSupport hceObj = (HashCodeEqualsSupport) obj;
//		
//		// if both object references are 
//		// referring to the same object.  
//		if (this == obj)
//			return true;
//		
//		// if both object id same.
//		if (id == hceObj.id)
//			return true;
//		
//		
//		return false;
//	}
//
//	@Override
//	public int hashCode() {
//		return this.id;
//	}

}
