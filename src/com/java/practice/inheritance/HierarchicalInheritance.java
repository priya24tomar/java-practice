package com.java.practice.inheritance;

public class HierarchicalInheritance extends Sound {

	public static void main(String[] args) {
		HierarchicalInheritance hi = new HierarchicalInheritance();
		System.out.println("Sound from other main class : " + hi.dog);
		AnimalSound as = new AnimalSound();
		as.method();
	}

}

class AnimalSound extends Sound {
	void method() {
		AnimalSound as = new AnimalSound();
		System.out.println("Sound from other class : " + as.dog);
	}
}