package com.java.practice.inheritance;

public class SingleInharitance extends Sound {
	public static void main(String[] args) {
		SingleInharitance si = new SingleInharitance();
		System.out.println(si.dog);
	}
}

class Sound {
	String dog = "Barking";

}